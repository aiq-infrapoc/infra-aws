resource "aws_instance" "jumphost" {
  ami           = "ami-019f9b3318b7155c5"
  instance_type = "t2.micro"
  //count = 1
  associate_public_ip_address = true
  subnet_id = aws_subnet.public_sn_1.id
  vpc_security_group_ids = [aws_security_group.jumphost-sg.id]
  #vpc_id = aws_vpc.vpc.id
  key_name = "eks-poc"

  user_data = <<EOF
#!/bin/bash
echo "kubectl setup"
cd /home/ec2-user
curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.28.5/2024-01-04/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH
echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
echo "kubectl setup is completed"

echo "awscli setup"
cd /home/ec2-user
mkdir awscli && cd awscli
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
echo "awscli setup is completed"


EOF
  
  tags = {
    Name = "eks_jumphost"
  }
}