variable "aws_region" {
  default = "us-east-2"
  description = "aws region"
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "default CIDR range of the VPC"
}

variable "pubsn_cidr" {
  default     = "10.0.101.0/24"
}

variable "prisn1_cidr" {
  default     = "10.0.1.0/24"
}

variable "prisn2_cidr" {
  default     = "10.0.2.0/24"
}

variable "prisn3_cidr" {
  default     = "10.0.3.0/24"
}

variable "cidr_all" {
  default     = "0.0.0.0/0"
}

variable "availability_zone1" {
  default     = "us-east-2a"
}

variable "availability_zone2" {
  default     = "us-east-2b"
}

variable "availability_zone3" {
  default     = "us-east-2c"
}